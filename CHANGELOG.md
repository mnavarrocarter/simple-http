# CHANGELOG

## v0.1.0 (11.12.2018)
This is a beta release. It is fully functional, but still misses some features
to reach 1.0.0.

- Implemented Header Bag
- Implemented Immutable Response
- Implemented Request
- Implemented Query Bag

## v0.2.0 (11.12.2018)
This add the RequestSender interface, to modify and decorate the way Requests are
sent. This allows handling of exceptions and implementing caching mechanisms.

- Added the RequestSender interface.
- Removed request sending logic to StreamRequestSender.

## v0.3.0 (17.12.2018)
This adds an interface to Responses, as well as a cache decorator for senders.

- Created Response interface.
- Responses can be serialized and deserialized.
- Added Numeric Filter
- Added SimpleCacheRequestSender.