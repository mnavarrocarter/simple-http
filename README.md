Simple HTTP
===========

Simple HTTP abstraction/client for your PHP projects using simple php streams.

## Installation

This library is in composer. You can install the latest stable version with:

```shell
$ composer require mnavarro/simple-http
```

## Usage
This library's purpose is to simply model HTTP Requests and Responses in an Object
Oriented way. Everything is done in the request class:


### Creating a Request

```php
<?php

use MNC\SimpleHttp\Request;

// You can create a new request passing a method and a uri
$request = new Request('GET', 'https://example.com');

// There are several factory methods for creating requests.

Request::get('http://some-url.com'); // Simple GET Request
Request::get('http://some-url.com', ['limit' => 20]); // With query params.
Request::post('http://some-url.com'); // A simple POST Request
Request::postForm('http://some-url.com', ['email' => 'some@email.com', 'password' => 'secretpass']); // POST Request with form data.
Request::postJson('http://some-url.com', ['email' => 'some@email.com', 'password' => 'secretpass']); // The same but in json

// You get the idea :)

// You can also modify the request after is created:

$request->addQuery('limit', 20); // Adds a query param
$request->putQueries(['limit' => 20, 'offset' => 0]); // Adds many query params.
$request->addHeader('Content-Type', 'my-fancy/content-type');

// There are also several methods for common operations on headers
$request->authorization('Bearer <token>'); // Adds authorization header
$request->contentType('application/json'); // Adds content-type header.
$request->userAgent('you custom user agent'); // Adds user-agent header.
$request->accept('application/json'); // Add accept header.
```

### Getting a Response

Once you have your request, is easy to get a response.

```php
<?php

use MNC\SimpleHttp\Request;

$request = new Request('GET', 'https://example.com');

// Your request can send itself.
$response = $request->send();

// Then you can do a lot of things with the response

$response->getStatus(); // The status code of the response.
$response->getBody(); // Gets the body as a php resource (stream)
$response->toArray(); // Converts the response into an array if it cans.
(string) $response; // Gets the response body as a string.

// These useful header methods help to check for header info:
$response->getHeader('Content-Type');
$response->containsHeader('Content-Type', 'application/json');
$response->hasHeader('Content-Type');
$response->getHeaders();
```

## Future Features
- Cookies Support
- BodyParsers
- Raw Http Printers and Parser

### Why This Library: Best Practices & Recomendations

TL;DR: It does not matter which HTTP client you use if you isolate HTTP jargon from your business logic.

Http Abstractions have proven really useful in the PHP world, so much, that a number
of PSRs in the PHP community have emerged in order to standardize the way we interact
with the HTTP protocol in our applications. In my opinion, however, PSR HTTP Interfaces
are too leaky, and they impose a very strict way of implementing them. Also, they try to
focus in two aspects of HTTP: the server and the client.

While the interfaces for servers are incredibly useful, I don't have the same opinion
on the ones for clients. At the end of the day, I always wrap a PSR Client Implementation
behind another class in my project that does not speak HTTP. All the HTTP jargon is
excluded from my application anyway, independent of the client I use. If I want to swap a
client for another, I can create another adapter for it that implements the contract that my
application speaks. That, for me, means that the HTTP client of my choice is no longer
going to be the most interoperable, but probably the most lightweight. 

Simple HTTP is my desire of such a client. It uses native PHP streams, so no curl
and no highly-bloated Guzzle are necessary in my dependencies. However, keep in mind
that this is a very basic client. If your application needs an advanced HTTP communication
layer, you'll probably be better off using Guzzle. I made this just to be able to do
simple HTTP Requests using PHP streams.

If you want to use this library, make sure you use it well, like any other HTTP library.
Rule of thumb number one: "You business logic MUST not know any of HTTP". That means,
no HTTP jargon, classes or references in your code. The only place where you should
have that, is in a class that implements an interface defined for you to work with
http.

Example:

```php
<?php

use MNC\SimpleHttp\Request;

interface CountryRepository
{
    /**
     * @return Country[]
     */
    public function getCountries(): array;
}

/**
 * This is the only class that can "know" of HTTP.
 */
class HTTPCountryRepository implements CountryRepository
{
    /**
     * @return Country[]
     */
    public function getCountries(): array
    {
        // What a beautiful one-liner :)
        $countries = [];
        $data = Request::get('https://restcountries.eu/rest/v2/all')->send()->toArray();
        foreach ($data as $entry) {
            $countries[] = Country::fromArray($entry);
        }
        return $countries;
    }
}

```

All feature requests, suggestions and contributions are highly welcomed.