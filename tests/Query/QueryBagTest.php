<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Tests\Query;

use MNC\SimpleHttp\Query\QueryBag;
use PHPUnit\Framework\TestCase;

class QueryBagTest extends TestCase
{
    /**
     * @var QueryBag()
     */
    private $query;

    public function setUp(): void
    {
        $this->query = new QueryBag();
    }

    public function testThatAddsQuery(): void
    {
        $this->query->add('limit', 20);
        $this->query->add('offset', 0);

        $this->assertTrue($this->query->has('limit'));
        $this->assertTrue($this->query->has('offset'));
        $this->assertSame(20, $this->query->get('limit'));
        $this->assertSame(0, $this->query->get('offset'));
        $this->assertNull($this->query->get('hello'));
    }

    public function testThatPutsQueries(): void
    {
        $this->query->put([
            'limit' => 20,
            'offset' => 0,
        ]);

        $this->assertTrue($this->query->has('limit'));
        $this->assertTrue($this->query->has('offset'));
        $this->assertSame(20, $this->query->get('limit'));
        $this->assertSame(0, $this->query->get('offset'));
        $this->assertNull($this->query->get('hello'));
    }

    public function testThatPutsNullValue(): void
    {
        $this->query->add('active');

        $this->assertTrue($this->query->has('active'));
        $this->assertNull($this->query->get('active'));
    }

    public function testStringConversion(): void
    {
        $this->query->add('limit', 20);
        $this->query->add('offset', 0);

        $this->assertSame('?limit=20&offset=0', (string) $this->query);
    }

    public function testStringConversionWithBooleans(): void
    {
        $this->query->add('size', 20);
        $this->query->add('active', true);

        $this->assertSame('?size=20&active=true', (string) $this->query);
    }

    public function testStringConversionWithNulls(): void
    {
        $this->query->add('size', 20);
        $this->query->add('active', null);

        $this->assertSame('?size=20&active', (string) $this->query);
    }
}
