<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Tests\Header;

use MNC\SimpleHttp\Header\HeaderBag;
use PHPUnit\Framework\TestCase;

class HeaderBagTest extends TestCase
{
    /**
     * @var HeaderBag
     */
    private $headers;

    public function setUp(): void
    {
        $this->headers = new HeaderBag();
    }

    public function testThatAddsHeader(): void
    {
        $this->headers->add('CONTENT-TYPE', 'application/json');
        $this->assertTrue($this->headers->has('Content-Type'));
        $this->assertTrue($this->headers->contains('Content-Type', 'application/json'));
    }

    public function testThatPutsHeader(): void
    {
        $this->headers->add('CONTENT-TYPE', 'application/json');
        $this->headers->put([
            'content-type' => 'text/csv',
            'user-agent' => 'mozilla ',
        ]);

        $this->assertTrue($this->headers->has('Content-Type'));
        $this->assertTrue($this->headers->contains('Content-Type', 'text/csv'));
        $this->assertTrue($this->headers->has('User-Agent'));
        $this->assertTrue($this->headers->contains('User-Agent', 'mozilla'));
    }

    public function testFetchAll(): void
    {
        $this->headers->add('CONTENT-TYPE', 'application/json');
        $this->headers->put([
            'content-type' => 'text/csv',
            'user-agent' => 'mozilla ',
        ]);

        $headers = $this->headers->all();

        $this->assertArrayHasKey('Content-Type', $headers);
        $this->assertArrayHasKey('User-Agent', $headers);
    }

    public function testToStringTransform(): void
    {
        $this->headers->add('CONTENT-TYPE', 'application/json');
        $this->headers->put([
            'content-type' => 'text/csv',
            'user-agent' => 'mozilla ',
            'location' => '/var/someplace',
        ]);

        $string = <<<EOF
Content-Type: text/csv
User-Agent: mozilla
Location: /var/someplace

EOF;

        $this->assertSame($string, $this->headers->__toString());
    }
}
