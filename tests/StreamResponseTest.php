<?php

namespace MNC\SimpleHttp\Tests;

use MNC\SimpleHttp\Response;
use MNC\SimpleHttp\StreamResponse;
use PHPUnit\Framework\TestCase;

/**
 * Class StreamResponseTest
 * @package MNC\SimpleHttp\Tests
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class StreamResponseTest extends TestCase
{

    public function testCreationFromString(): void
    {
        $response = StreamResponse::createFromStringRepresentation(file_get_contents(__DIR__.'/response'));

        $this->assertInstanceOf(Response::class, $response);
    }
}
