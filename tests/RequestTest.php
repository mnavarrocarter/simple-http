<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Tests;

use MNC\SimpleHttp\Request;
use MNC\SimpleHttp\StreamResponse;
use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{
    /**
     * @see http://example.com/
     */
    public function testSimpleGet(): void
    {
        $response = Request::get('http://example.com')->send();

        $this->assertTrue($response->containsHeader('Content-Type', 'text/html; charset=UTF-8'));
        $this->assertContains('Example Domain', (string) $response);
    }

    /**
     * @see https://www.md5hashgenerator.com/
     */
    public function testPostForm(): void
    {
        $response = Request::postForm('https://www.md5hashgenerator.com', ['stringy' => 'hello', 'action' => 'Encode'])->send();

        $this->assertContains('5d41402abc4b2a76b9719d911017c592', (string) $response);
    }

    public function testResponseJsonPayload(): void
    {
        $response = Request::get('https://restcountries.eu/rest/v2/all')->send();
        $data = $response->toArray();
        $this->assertCount(250, $data);
    }

    /**
     * @see http://example.com/
     */
    public function testInvalidJsonResponse(): void
    {
        $response = Request::get('http://example.com')->send();
        $this->assertEmpty($response->toArray());
    }

    public function testSerialization(): void
    {
        $response = Request::get('http://example.com')->send();

        $serializedResponse = serialize($response);

        $this->assertIsString('string', $serializedResponse);

        $this->assertInstanceOf(StreamResponse::class, unserialize($serializedResponse, [StreamResponse::class]));
    }
}
