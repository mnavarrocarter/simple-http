<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Sender;

use MNC\SimpleHttp\Exception\ClientException;
use MNC\SimpleHttp\Exception\ConnectionException;
use MNC\SimpleHttp\Exception\ServerException;
use MNC\SimpleHttp\Request;
use MNC\SimpleHttp\Response;
use MNC\SimpleHttp\StreamResponse;

/**
 * Class StreamRequestSender.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class StreamRequestSender implements RequestSender
{
    /**
     * @var bool
     */
    private $followRedirects = false;
    /**
     * @var int
     */
    private $maxRedirects = 20;
    /**
     * @var int
     */
    private $timeout = 60;
    /**
     * @var bool
     */
    private $throwExceptionOn500 = true;
    /**
     * @var bool
     */
    private $throwExceptionOn400 = true;

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function send(Request $request): Response
    {
        $stream = @fopen(
            $this->appendQueriesToUri($request),
            'rb', // Binary safe read
            false,
            stream_context_create($this->appendHeadersToContext($request, $this->buildContext($request)))
        );

        if (false === $stream) {
            throw new ConnectionException(sprintf('Could not connect to %s. Either the host is down or is not resolvable.', $request->getUri()));
        }

        $response = StreamResponse::createFromStream($stream);

        if ($this->throwExceptionOn500 && $response->getStatus() >= 500) {
            throw new ServerException($response);
        }
        if ($this->throwExceptionOn400 && $response->getStatus() >= 400) {
            throw new ClientException($response);
        }

        return $response;
    }

    /**
     * @return bool
     */
    public function isFollowRedirects(): bool
    {
        return $this->followRedirects;
    }

    /**
     * @return int
     */
    public function getMaxRedirects(): int
    {
        return $this->maxRedirects;
    }

    /**
     * @return int
     */
    public function getTimeout(): int
    {
        return $this->timeout;
    }

    /**
     * @return bool
     */
    public function isThrowExceptionOn500(): bool
    {
        return $this->throwExceptionOn500;
    }

    /**
     * @return bool
     */
    public function isThrowExceptionOn400(): bool
    {
        return $this->throwExceptionOn400;
    }

    /**
     * @param bool $followRedirects
     *
     * @return StreamRequestSender
     */
    public function setFollowRedirects(bool $followRedirects): StreamRequestSender
    {
        $this->followRedirects = $followRedirects;

        return $this;
    }

    /**
     * @param int $maxRedirects
     *
     * @return StreamRequestSender
     */
    public function setMaxRedirects(int $maxRedirects): StreamRequestSender
    {
        $this->maxRedirects = $maxRedirects;

        return $this;
    }

    /**
     * @param int $timeout
     *
     * @return StreamRequestSender
     */
    public function setTimeout(int $timeout): StreamRequestSender
    {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * @param bool $throwExceptionOn500
     *
     * @return StreamRequestSender
     */
    public function setThrowExceptionOn500(bool $throwExceptionOn500): StreamRequestSender
    {
        $this->throwExceptionOn500 = $throwExceptionOn500;

        return $this;
    }

    /**
     * @param bool $throwExceptionOn400
     *
     * @return StreamRequestSender
     */
    public function setThrowExceptionOn400(bool $throwExceptionOn400): StreamRequestSender
    {
        $this->throwExceptionOn400 = $throwExceptionOn400;

        return $this;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function buildContext(Request $request): array
    {
        return [
            'http' => [
                'method' => $request->getMethod(),
                'user_agent' => 'MNC Simple Request 1.0.0',
                'ignore_errors' => true,
                'timeout' => $this->timeout,
                'protocol_version' => 1.1,
                'follow_location' => $this->followRedirects,
                'max_redirects' => $this->maxRedirects,
                'content' => $request->getBody(),
            ],
        ];
    }

    /**
     * @param Request $request
     * @param array   $context
     *
     * @return array
     */
    private function appendHeadersToContext(Request $request, array $context): array
    {
        $headers = [];
        foreach ($request->getHeaders() as $header => $value) {
            $headers[] = sprintf('%s: %s', $header, $value);
        }
        $context['http']['header'] = $headers;

        return $context;
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    private function appendQueriesToUri(Request $request): string
    {
        return $request->getUri().$request->getQueryString();
    }
}
