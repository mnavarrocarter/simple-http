<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Sender;

use MNC\SimpleHttp\Exception\ClientException;
use MNC\SimpleHttp\Exception\ConnectionException;
use MNC\SimpleHttp\Exception\ServerException;
use MNC\SimpleHttp\Request;
use MNC\SimpleHttp\Response;

/**
 * Interface RequestSender.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface RequestSender
{
    /**
     * @param Request $request
     *
     * @return Response
     *
     * @throws ConnectionException when a connection issue occurs
     * @throws ServerException     when the server returns a 500 status code
     * @throws ClientException     when the server returns a 400 status code
     */
    public function send(Request $request): Response;
}
