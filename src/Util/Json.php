<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Util;

/**
 * Class Json.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class Json
{
    /**
     * @param array $data
     *
     * @return string
     */
    public static function encode(array $data): string
    {
        return json_encode($data);
    }

    /**
     * @param string $json
     *
     * @return array
     */
    public static function decode(string $json): array
    {
        $result = json_decode($json, true);
        if (!$result) {
            throw new JsonException('Json decode error: '.json_last_error_msg());
        }

        return $result;
    }
}
