<?php

namespace MNC\SimpleHttp\Util;

use MNC\SimpleHttp\Request;
use MNC\SimpleHttp\Response;

/**
 * Class Formatter
 * @package MNC\SimpleHttp\Util
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class Formatter
{
    /**
     * @param Request $request
     *
     * @return string
     */
    public static function hashRequest(Request $request): string
    {
        return sha1(sprintf('%s %s%s', $request->getMethod(), $request->getUri(), $request->getQueryString()));
    }

    /**
     * @param Response $response
     *
     * @return string
     */
    public static function responseToString(Response $response): string
    {
        $str = '';
        $str .= sprintf('HTTP/%s %s %s%s', $response->getProtocolVersion(), $response->getStatus(), $response->getReasonPhrase(), PHP_EOL);
        foreach ($response->getHeaders() as $name => $value) {
            $str .= sprintf('%s: %s%s', $name, $value, PHP_EOL);
        }
        $str .= PHP_EOL;
        $str .= $response;
        return $str;
    }
}