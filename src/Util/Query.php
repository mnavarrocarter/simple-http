<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Util;

/**
 * Class Query.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class Query
{
    public const BOOLEAN_AS_TEXT = 1;
    public const INCLUDE_NULL_VALUES = 2;
    public const TRANSFORM_SPECIAL_CHARS = 4;

    /**
     * @param array $data
     * @param int   $options
     *
     * @return string
     */
    public static function encode(array $data, int $options = 7): string
    {
        $string = '';
        foreach ($data as $key => $value) {
            if (null === $value && !($options & self::INCLUDE_NULL_VALUES)) {
                continue;
            }
            if ($options & self::BOOLEAN_AS_TEXT && is_bool($value)) {
                $value = $value ? 'true' : 'false';
            }
            $string .= $key;
            if (null !== $value) {
                $string .= '='.$value;
            }
            $string .= '&';
        }

        $string = trim($string, '&');

        if ($options & self::TRANSFORM_SPECIAL_CHARS) {
            // Add more special characters
            return str_replace(['[', ']', '-'], ['%5B', '%5D', '%2F'], $string);
        }

        return $string;
    }
}
