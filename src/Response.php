<?php

namespace MNC\SimpleHttp;

use MNC\SimpleHttp\Header\ReadsHeaders;

/**
 * Interface Response
 * @package MNC\SimpleHttp
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface Response extends ReadsHeaders
{
    /**
     * Gets the body as a PHP resource.
     *
     * If you want a string representation, cast the response into a string.
     *
     * @return resource
     */
    public function getBody();

    /**
     * Converts the response body into an array if encoded in json.
     *
     * If body is not a json string, then an empty array is returned.
     *
     * @return array
     */
    public function toArray(): array;

    /**
     * Returns the HTTP Status Code.
     *
     * @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
     *
     * @return int
     */
    public function getStatus(): int;

    /**
     * Returns the HTTP Reason Phrase.
     *
     * @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
     *
     * @return string
     */
    public function getReasonPhrase(): string;

    /**
     * Gets the HTTP protocol version of this response.
     *
     * @return float
     */
    public function getProtocolVersion(): float;

    /**
     * @return string
     */
    public function __toString(): string;
}