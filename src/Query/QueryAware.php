<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Query;

/**
 * Interface QueryAware.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface QueryAware
{
    /**
     * @param string $key
     * @param        $value
     *
     * @return $this
     */
    public function addQuery(string $key, $value = null): self;

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasQuery(string $key): bool;

    /**
     * @param string $key
     * @param null   $fallback
     *
     * @return mixed
     */
    public function getQuery(string $key, $fallback = null);

    /**
     * @param array $keyValueMap
     *
     * @return $this
     */
    public function putQueries(array $keyValueMap): self;

    /**
     * @return array
     */
    public function getQueries(): array;

    /**
     * @return string
     */
    public function getQueryString(): string;
}
