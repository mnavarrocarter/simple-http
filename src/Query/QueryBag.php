<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Query;

use MNC\SimpleHttp\Util\Query;

/**
 * Class QueryBag.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class QueryBag
{
    /**
     * @var array
     */
    private $queries;

    /**
     * QueryBag constructor.
     */
    public function __construct()
    {
        $this->queries = [];
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return '?'.Query::encode($this->queries);
    }

    /**
     * @param string $key
     * @param        $value
     *
     * @return $this
     */
    public function add(string $key, $value = null): self
    {
        $this->queries[$key] = $value;

        return $this;
    }

    public function has(string $key): bool
    {
        return array_key_exists($key, $this->queries);
    }

    public function get(string $key, $fallback = null)
    {
        return $this->queries[$key] ?? $fallback;
    }

    /**
     * @param array $keyValueMap
     *
     * @return $this
     */
    public function put(array $keyValueMap): self
    {
        $this->queries = array_merge($this->queries, $keyValueMap);

        return $this;
    }

    public function all(): array
    {
        return $this->queries;
    }
}
