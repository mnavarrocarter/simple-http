<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp;

/**
 * Trait ReadHeaders.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
trait ReadHeaders
{
    /**
     * @var array
     */
    protected $headers = [];

    public function hasHeader(string $key): bool
    {
        return array_key_exists(mb_strtolower($key), $this->headers);
    }

    public function getHeader(string $key, $fallback = null)
    {
        return $this->headers[mb_strtolower($key)] ?? $fallback;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }
}
