<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp;

/**
 * Trait Headers.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
trait Headers
{
    /**
     * @var array
     */
    protected $headers = [];

    /**
     * @param string $key
     * @param        $value
     *
     * @return $this
     */
    public function addHeader(string $key, $value): self
    {
        $this->headers[mb_strtolower($key)] = $value;

        return $this;
    }

    public function hasHeader(string $key): bool
    {
        return array_key_exists(mb_strtolower($key), $this->headers);
    }

    public function getHeader(string $key, $fallback = null)
    {
        return $this->headers[mb_strtolower($key)] ?? $fallback;
    }

    /**
     * @param array $keyValueMap
     *
     * @return $this
     */
    public function putHeaders(array $keyValueMap): self
    {
        array_walk($keyValueMap, function ($val, $key) { mb_strtolower($key); });
        $this->headers = array_merge($this->headers, $keyValueMap);

        return $this;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }
}
