<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Exception;

use MNC\SimpleHttp\StreamResponse;

/**
 * Thrown when a 400 error occurs.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ClientException extends HttpException
{
    /**
     * @var StreamResponse
     */
    private $response;

    /**
     * ServerException constructor.
     *
     * @param StreamResponse $response
     */
    public function __construct(StreamResponse $response)
    {
        $this->response = $response;
        parent::__construct(sprintf(
            'The server has responded with a %s error that says: %s',
            $response->getStatus(),
            (string) $response
        ), $response->getStatus());
    }

    /**
     * @return StreamResponse
     */
    public function getResponse(): StreamResponse
    {
        return $this->response;
    }
}
