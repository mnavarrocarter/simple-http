<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Exception;

/**
 * Class HttpException.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class HttpException extends \RuntimeException
{
}
