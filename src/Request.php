<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp;

use MNC\SimpleHttp\Header\HeaderBag;
use MNC\SimpleHttp\Header\HeaderOperations;
use MNC\SimpleHttp\Header\ModifiesHeaders;
use MNC\SimpleHttp\Header\ReadsHeaders;
use MNC\SimpleHttp\Query\QueryAware;
use MNC\SimpleHttp\Query\QueryBag;
use MNC\SimpleHttp\Sender\RequestSender;
use MNC\SimpleHttp\Sender\StreamRequestSender;

/**
 * Class Request.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class Request implements ReadsHeaders, ModifiesHeaders, QueryAware
{
    use HeaderOperations;

    /**
     * @var string
     */
    protected $uri;
    /**
     * @var QueryBag
     */
    protected $query;
    /**
     * @var string
     */
    protected $body;
    /**
     * @var string
     */
    protected $method;
    /**
     * @var HeaderBag
     */
    protected $headers;

    /**
     * StreamRequest constructor.
     *
     * @param string $method
     * @param string $uri
     * @param string $body
     * @param array  $queries
     * @param array  $headers
     */
    public function __construct(string $method, string $uri, string $body = '', array $queries = [], array $headers = [])
    {
        $this->method = $method;
        $this->uri = $uri;
        $this->body = $body;
        $this->query = (new QueryBag())->put($queries);
        $this->headers = (new HeaderBag())->put($headers);
    }

    /**
     * @param string $uri
     * @param array  $queries
     * @param array  $headers
     *
     * @return Request
     */
    public static function get(string $uri, array $queries = [], array $headers = []): Request
    {
        return new self('GET', $uri, '', $queries, $headers);
    }

    /**
     * @param string $uri
     * @param array  $data
     * @param array  $queries
     * @param array  $headers
     *
     * @return Request
     */
    public static function postJson(string $uri, array $data, array $queries = [], array $headers = []): Request
    {
        $request = new self('POST', $uri, json_encode($data), $queries, $headers);
        $request->addHeader('Content-Type', ContentType::JSON);

        return $request;
    }

    /**
     * @param string $uri
     * @param array  $data
     * @param array  $queries
     * @param array  $headers
     *
     * @return Request
     */
    public static function postForm(string $uri, array $data, array $queries = [], array $headers = []): Request
    {
        $request = new self('POST', $uri, http_build_query($data), $queries, $headers);
        $request->addHeader('Content-Type', ContentType::FORM);

        return $request;
    }

    /**
     * @param string $uri
     * @param string $body
     * @param array  $queries
     * @param array  $headers
     *
     * @return Request
     */
    public static function post(string $uri, string $body = '', array $queries = [], array $headers = []): Request
    {
        return new self('POST', $uri, $body, $queries, $headers);
    }

    /**
     * @param string $uri
     * @param string $body
     * @param array  $queries
     * @param array  $headers
     *
     * @return Request
     */
    public static function put(string $uri, string $body = '', array $queries = [], array $headers = []): Request
    {
        return new self('PUT', $uri, $body, $queries, $headers);
    }

    /**
     * @param string $uri
     * @param array  $data
     * @param array  $queries
     * @param array  $headers
     *
     * @return Request
     */
    public static function putJson(string $uri, array $data, array $queries = [], array $headers = []): Request
    {
        return new self('PUT', $uri, json_encode($data), $queries, $headers);
    }

    /**
     * @param string $uri
     * @param array  $queries
     * @param array  $headers
     *
     * @return Request
     */
    public static function delete(string $uri, array $queries = [], array $headers = []): Request
    {
        return new self('DELETE', $uri, '', $queries, $headers);
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function setBody(string $body): Request
    {
        $this->body = $body;

        return $this;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    /**
     * @param RequestSender|null $sender
     *
     * @return Response
     */
    public function send(RequestSender $sender = null): Response
    {
        if (null === $sender) {
            $sender = new StreamRequestSender();
        }

        return $sender->send($this);
    }

    /**
     * @param bool $bool
     *
     * @return Request
     */
    public function followRedirects(bool $bool): Request
    {
        $this->followRedirects = $bool;

        return $this;
    }

    /**
     * @param int $number
     *
     * @return Request
     */
    public function setMaxRedirects(int $number): Request
    {
        $this->maxRedirects = $number;

        return $this;
    }

    /**
     * @param string $key
     * @param        $value
     *
     * @return Request
     */
    public function addHeader(string $key, $value): ModifiesHeaders
    {
        $this->headers->add($key, $value);

        return $this;
    }

    /**
     * @param array $keyValueMap
     *
     * @return Request
     */
    public function putHeaders(array $keyValueMap): ModifiesHeaders
    {
        $this->headers->put($keyValueMap);

        return $this;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasHeader(string $key): bool
    {
        return $this->headers->has($key);
    }

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return bool
     */
    public function containsHeader(string $key, $value): bool
    {
        return $this->headers->contains($key, $value);
    }

    /**
     * @param string $key
     * @param null   $fallback
     *
     * @return mixed
     */
    public function getHeader(string $key, $fallback = null)
    {
        return $this->headers->get($key, $fallback);
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers->all();
    }

    /**
     * @param string $key
     * @param null   $value
     *
     * @return Request
     */
    public function addQuery(string $key, $value = null): QueryAware
    {
        $this->query->add($key, $value);

        return $this;
    }

    public function hasQuery(string $key): bool
    {
        return $this->query->has($key);
    }

    /**
     * @param string $key
     * @param null   $fallback
     *
     * @return mixed|null
     */
    public function getQuery(string $key, $fallback = null)
    {
        return $this->query->get($key, $fallback);
    }

    /**
     * @param array $keyValueMap
     *
     * @return Request
     */
    public function putQueries(array $keyValueMap): QueryAware
    {
        $this->query->put($keyValueMap);

        return $this;
    }

    /**
     * @return array
     */
    public function getQueries(): array
    {
        return $this->query->all();
    }

    public function getQueryString(): string
    {
        return (string) $this->query;
    }
}
