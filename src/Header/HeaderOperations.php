<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Header;

/**
 * Trait HeaderOperations.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
trait HeaderOperations
{
    /**
     * @param string $value
     *
     * @return $this
     */
    public function userAgent(string $value): self
    {
        return $this->addHeader('User-Agent', $value);
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function authorization(string $value): self
    {
        return $this->addHeader('Authorization', $value);
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function contentType(string $value): self
    {
        return $this->addHeader('Content-Type', $value);
    }

    /**
     * @param string $value
     *
     * @return $this
     */
    public function accept(string $value): self
    {
        return $this->addHeader('Accept', $value);
    }
}
