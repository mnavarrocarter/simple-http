<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Header;

/**
 * Interface ReadsHeaders.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface ReadsHeaders
{
    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasHeader(string $key): bool;

    /**
     * @param string $key
     * @param null   $fallback
     *
     * @return mixed
     */
    public function getHeader(string $key, $fallback = null);

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return bool
     */
    public function containsHeader(string $key, $value): bool;

    /**
     * @return array
     */
    public function getHeaders(): array;
}
