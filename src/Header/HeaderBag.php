<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Header;

use MNC\SimpleHttp\Filter\ChainFilter;
use MNC\SimpleHttp\Filter\Filter;
use MNC\SimpleHttp\Filter\HeaderFilter;
use MNC\SimpleHttp\Filter\NumericFilter;
use MNC\SimpleHttp\Filter\TrimFilter;

/**
 * Class HeaderBag.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class HeaderBag
{
    /**
     * @var array
     */
    private $headers;
    /**
     * @var array
     */
    private $meta;
    /**
     * @var Filter
     */
    private $keyFilter;
    /**
     * @var Filter|null
     */
    private $valueFilter;

    /**
     * HeaderBag constructor.
     *
     * @param Filter|null $keyFilter
     * @param Filter|null $valueFilter
     */
    public function __construct(Filter $keyFilter = null, Filter $valueFilter = null)
    {
        $this->headers = [];
        $this->meta = [];
        $this->keyFilter = $keyFilter ?? new ChainFilter(new TrimFilter(), new HeaderFilter());
        $this->valueFilter = $valueFilter ?? new ChainFilter(new TrimFilter(),  new NumericFilter());
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $string = '';
        foreach ($this->headers as $name => $value) {
            $string .= sprintf('%s: %s%s', $name, $value, PHP_EOL);
        }

        return $string;
    }

    /**
     * @param string $key
     * @param        $value
     * @param array  $meta
     *
     * @return $this
     */
    public function add(string $key, $value, array $meta = []): self
    {
        $this->headers[$this->keyFilter->apply($key)] = $this->valueFilter->apply($value);
        foreach ($meta as $metaKey => $metaValue) {
            $this->meta[$this->keyFilter->apply($key)][$this->keyFilter->apply($metaKey)] = $this->valueFilter->apply($metaValue);
        }

        return $this;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool
    {
        return array_key_exists($this->keyFilter->apply($key), $this->headers);
    }

    /**
     * @param string $key
     * @param null   $fallback
     *
     * @return mixed
     */
    public function get(string $key, $fallback = null)
    {
        return $this->headers[$this->keyFilter->apply($key)] ?? $fallback;
    }

    /**
     * @param array $keyValueMap
     *
     * @return $this
     */
    public function put(array $keyValueMap): self
    {
        foreach ($keyValueMap as $key => $value) {
            $this->add($key, $value);
        }

        return $this;
    }

    /**
     * @param string $key
     * @param        $value
     *
     * @return bool
     */
    public function contains(string $key, $value): bool
    {
        if (!$this->has($key)) {
            return false;
        }

        return $this->headers[$this->keyFilter->apply($key)] === $this->valueFilter->apply($value);
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->headers;
    }
}
