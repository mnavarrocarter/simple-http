<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Header;

/**
 * Interface ModifiesHeaders.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface ModifiesHeaders
{
    /**
     * @param string $key
     * @param        $value
     *
     * @return $this
     */
    public function addHeader(string $key, $value): self;

    /**
     * @param array $keyValueMap
     *
     * @return $this
     */
    public function putHeaders(array $keyValueMap): self;
}
