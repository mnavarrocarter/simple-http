<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp;

use MNC\SimpleHttp\Header\HeaderBag;
use MNC\SimpleHttp\Util\Json;
use MNC\SimpleHttp\Util\JsonException;

/**
 * Class StreamResponse.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class StreamResponse implements Response, \Serializable
{
    /**
     * @var resource
     */
    private $stream;
    /**
     * @var int
     */
    private $status;
    /**
     * @var string
     */
    private $reasonPhrase;
    /**
     * @var float
     */
    private $protocolVersion;
    /**
     * @var HeaderBag
     */
    private $headers;

    private function __construct()
    {
        $this->headers = new HeaderBag();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $data = stream_get_contents($this->stream);
        if (is_string($data)) {
            return $data;
        }

        return '';
    }

    /**
     * @param string $string
     *
     * @return StreamResponse
     */
    public static function createFromStringRepresentation(string $string): StreamResponse
    {
        $self = new self();

        $inBody = false;
        $body = '';
        $data = explode(PHP_EOL, $string);

        foreach ($data as $key => $line) {
            if ($line === '' && $inBody === false) {
                $inBody = true;
                continue;
            }
            if (0 === $key) {
                $self->parseFirstLine($line);
                continue;
            }
            if ($inBody === false) {
                $self->parseHeaderLine($line);
                continue;
            }
            $body .= $line;
        }
        $self->stream = fopen('php://memory', 'r+b');
        fwrite($self->stream, $body);
        rewind($self->stream);

        return $self;
    }

    /**
     * @param resource $stream
     *
     * @return StreamResponse
     */
    public static function createFromStream($stream): StreamResponse
    {
        $self = new self();
        $self->stream = $stream;

        $metadata = stream_get_meta_data($self->stream);
        foreach ($metadata['wrapper_data'] as $key => $data) {
            if (0 === $key) {
                $self->parseFirstLine($data);
                continue;
            }
            $self->parseHeaderLine($data);
        }

        return $self;
    }

    /**
     * Gets the body as a PHP resource.
     *
     * If you want a string representation, cast the response into a string.
     *
     * @return resource
     */
    public function getBody()
    {
        return $this->stream;
    }

    /**
     * Converts the response body into an array if encoded in json.
     *
     * If body is not a json string, then an empty array is returned.
     *
     * @return array
     */
    public function toArray(): array
    {
        try {
            return Json::decode((string) $this);
        } catch (JsonException $e) {
            return [];
        }
    }

    /**
     * Returns the HTTP Status Code.
     *
     * @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
     *
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * Returns the HTTP Reason Phrase.
     *
     * @see https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
     *
     * @return string
     */
    public function getReasonPhrase(): string
    {
        return $this->reasonPhrase;
    }

    /**
     * Gets the HTTP protocol version of this response.
     *
     * @return float
     */
    public function getProtocolVersion(): float
    {
        return $this->protocolVersion;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasHeader(string $key): bool
    {
        return $this->headers->has($key);
    }

    /**
     * @param string $key
     * @param null   $fallback
     *
     * @return mixed
     */
    public function getHeader(string $key, $fallback = null)
    {
        return $this->headers->get($key, $fallback);
    }

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return bool
     */
    public function containsHeader(string $key, $value): bool
    {
        return $this->headers->contains($key, $value);
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers->all();
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return base64_encode(json_encode([
            'stream' => (string) $this,
            'protocolVersion' => $this->protocolVersion,
            'status' => $this->status,
            'reasonPhrase' => $this->reasonPhrase,
            'headers' => $this->headers->all(),
        ]));
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized): void
    {
        $array = json_decode(base64_decode($serialized), true);
        $this->stream = fopen('php://memory', 'rb+');
        fwrite($this->stream, $array['stream']);
        rewind($this->stream);
        $this->status = $array['status'];
        $this->reasonPhrase = $array['reasonPhrase'];
        $this->protocolVersion = $array['protocolVersion'];
        $this->headers = new HeaderBag();
        $this->headers->put($array['headers']);
    }

    /**
     * @param string $line
     */
    private function parseFirstLine(string $line): void
    {
        [$protocol, $status, $reason] = explode(' ', $line, 3);
        $this->protocolVersion = (float) str_replace('HTTP/', '', $protocol);
        $this->status = (int) $status;
        $this->reasonPhrase = $reason;
    }

    /**
     * @param string $line
     */
    private function parseHeaderLine(string $line): void
    {
        [$name, $value] = (array_map(function ($x) { return array_map('trim', explode(':', $x, 2)); }, array_filter(array_map('trim', explode("\n", $line)))))[0];

        $this->headers->add($name, $value);
    }
}
