<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp;

/**
 * Class ContentType.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ContentType
{
    public const JSON = 'application/json';
    public const CSV = 'text/csv';
    public const XML = 'application/xml';
    public const FORM = 'application/x-www-form-urlencoded';
}
