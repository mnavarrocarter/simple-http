<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Filter;

/**
 * Class ChainFilter.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class ChainFilter implements Filter
{
    /**
     * @var Filter[]
     */
    private $filters;

    public function __construct(Filter ...$filters)
    {
        $this->filters = $filters;
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function apply($value)
    {
        foreach ($this->filters as $filter) {
            $value = $filter->apply($value);
        }

        return $value;
    }
}
