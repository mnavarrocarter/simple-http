<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Filter;

/**
 * Class HeaderFilter.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class HeaderFilter implements Filter
{
    /**
     * @param $value
     *
     * @return mixed
     */
    public function apply($value)
    {
        return implode('-', array_map([$this, 'transform'], explode('-', $value)));
    }

    /**
     * @param string $part
     *
     * @return string
     */
    public function transform(string $part): string
    {
        return ucfirst(strtolower($part));
    }
}
