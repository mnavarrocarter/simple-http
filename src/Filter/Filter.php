<?php

/*
 * This file is part of the MNC\SimpleHttp library.
 *
 * (c) Matías Navarro Carter <mnavarrocarter@gmail.com>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace MNC\SimpleHttp\Filter;

/**
 * Interface Filter.
 *
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
interface Filter
{
    /**
     * @param $value
     *
     * @return mixed
     */
    public function apply($value);
}
