<?php

namespace MNC\SimpleHttp\Filter;

/**
 * Class NumericFilter
 * @package MNC\SimpleHttp\Filter
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
class NumericFilter implements Filter
{
    /**
     * @param $value
     *
     * @return mixed
     */
    public function apply($value)
    {
        if (is_numeric($value)) {
            return (int) $value + 0;
        }
        return $value;
    }
}