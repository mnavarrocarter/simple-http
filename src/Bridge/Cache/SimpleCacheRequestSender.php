<?php

namespace MNC\SimpleHttp\Bridge\Cache;

use MNC\SimpleHttp\Request;
use MNC\SimpleHttp\Response;
use MNC\SimpleHttp\Sender\RequestSender;
use MNC\SimpleHttp\Sender\StreamRequestSender;
use MNC\SimpleHttp\StreamResponse;
use MNC\SimpleHttp\Util\Formatter;
use Psr\SimpleCache\CacheInterface;

/**
 * Class SimpleCacheRequestSender
 * @package MNC\SimpleHttp\Bridge\Cache
 * @author Matías Navarro Carter <mnavarrocarter@gmail.com>
 */
final class SimpleCacheRequestSender implements RequestSender
{
    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var RequestSender
     */
    private $sender;

    /**
     * SimpleCacheRequestSender constructor.
     *
     * @param CacheInterface $cache
     * @param RequestSender  $sender
     */
    public function __construct(CacheInterface $cache, RequestSender $sender)
    {
        $this->cache = $cache;
        $this->sender = $sender;
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function send(Request $request): Response
    {
        $hash = Formatter::hashRequest($request);

        if ($this->cache->has($hash)) {
            $response = $this->cache->get($hash);
            if ($response instanceof Response) {
                return $response;
            }
            return StreamResponse::createFromStringRepresentation($response);
        }
        $response = $this->sender->send($request);
        $this->cache->set($hash, Formatter::responseToString($response));

        return StreamResponse::createFromStringRepresentation($this->cache->get($hash));
    }
}